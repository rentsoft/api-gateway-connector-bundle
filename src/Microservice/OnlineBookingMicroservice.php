<?php

namespace Rentsoft\ApiGatewayConnectorBundle\Microservice;

use Doctrine\Common\Collections\ArrayCollection;
use Rentsoft\ApiGatewayConnectorBundle\Entity\OnlineBookingMicroservice\OnlineBooking\OnlineBooking;
use Rentsoft\ApiGatewayConnectorBundle\Extension\ApiGatewayHttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OnlineBookingMicroservice
{
    CONST URI_BASE = "/online-booking/v1";
    CONST URI_ONLINE_BOOKINGS = '/online-bookings';

    private ApiGatewayHttpClient $apiGatewayHttpClient;

    public function __construct($apiGatewayHttpClient)
    {
        $this->apiGatewayHttpClient = $apiGatewayHttpClient;
    }

    public function getOnlineBookingByUri(string $uri): ?OnlineBooking
    {
        $arr['query']['uri'] = $uri;
        $response = $this->apiGatewayHttpClient->request(Request::METHOD_GET, self::URI_BASE . self::URI_ONLINE_BOOKINGS, $arr);

        $totalItems = $response->toArray()['hydra:totalItems'];
        $array = $this->apiGatewayHttpClient->deserializeCollection(OnlineBooking::class, $response->toArray()['hydra:member'], $totalItems);

        if($totalItems != 1) {
            throw new NotFoundHttpException();
        }

        return $array['data'][0];
    }

    public function getOnlineBookingById(int $id): ?OnlineBooking
    {
        $response = $this->apiGatewayHttpClient->request(Request::METHOD_GET, self::URI_BASE . self::URI_ONLINE_BOOKINGS . '/' . $id);

        $item = $this->apiGatewayHttpClient->deserializeItem(OnlineBooking::class, $response->getContent());

        return $item;
    }

    public function getOnlineBookingByClientId(int $clientId): ArrayCollection
    {
        $arr['query']['client'] = $clientId;
        $response = $this->apiGatewayHttpClient->request(Request::METHOD_GET, self::URI_BASE . self::URI_ONLINE_BOOKINGS, $arr);

        $totalItems = $response->toArray()['hydra:totalItems'];
        $array = $this->apiGatewayHttpClient->deserializeCollection(OnlineBooking::class, $response->toArray()['hydra:member'], $totalItems);

        return $array;
    }

    public function getOnlineBookings(array $parameters): ArrayCollection
    {
        $arr['query'] = $parameters;
        $response = $this->apiGatewayHttpClient->request(Request::METHOD_GET, self::URI_BASE . self::URI_ONLINE_BOOKINGS, $arr);

        $totalItems = $response->toArray()['hydra:totalItems'];
        $array = $this->apiGatewayHttpClient->deserializeCollection(OnlineBooking::class, $response->toArray()['hydra:member'], $totalItems);

        return $array;
    }
}
