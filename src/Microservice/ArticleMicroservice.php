<?php

namespace Rentsoft\ApiGatewayConnectorBundle\Microservice;

use Doctrine\Common\Collections\ArrayCollection;
use Rentsoft\ApiGatewayConnectorBundle\Entity\ArticleMicroservice\Client\Client;
use Rentsoft\ApiGatewayConnectorBundle\Extension\ApiGatewayHttpClient;
use Symfony\Component\HttpFoundation\Request;

class ArticleMicroservice
{
    CONST URI_BASE = "/article/v1";
    CONST URI_ARTICLES = '/articles';
    CONST URI_CLIENTS = '/clients';

    private ApiGatewayHttpClient $apiGatewayHttpClient;

    public function __construct($apiGatewayHttpClient)
    {
        $this->apiGatewayHttpClient = $apiGatewayHttpClient;
    }


    public function getClients(array $parameters): ArrayCollection
    {
        $arr['query'] = $parameters;
        $response = $this->apiGatewayHttpClient->request(Request::METHOD_GET, self::URI_BASE . self::URI_CLIENTS, $arr);

        $totalItems = $response->toArray()['hydra:totalItems'];
        $array = $this->apiGatewayHttpClient->deserializeCollection(Client::class, $response->toArray()['hydra:member'], $totalItems);

        return $array;
    }
}
